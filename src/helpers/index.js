const transposeMatrix = (matrix) => {

    const matrixLength = matrix.length;
    const matrixT = [];
  
    //ALgorithm to inverse matrix (Transpose Matrix)
    for (let i = 0; i < matrixLength; i++) {
            let newRow = '';
            for (let j = 0; j < matrixLength; j++) {
                newRow += matrix[j][i];
            }
        matrixT.push(newRow);
    }

    return matrixT;
}
const reverseMatrix = (matrix) => {

    const reverseMatrix = matrix.map((row) => row.split('').join(''));

    return reverseMatrix;
}
const getDiagonals = (dna) => {

    const rows = dna.length;
    const columns = dna[0].length;

    let diagonals = [];
    let j, d;
    
    //First corner
    for(let i = 0; i < rows; i++) {
        let diagonal = '';
        for(j=i, d = 0; j >= 0; j--, d++ ) {
            diagonal += dna[j][d];
        }
        diagonals.push(diagonal);
    }

    //Second corner
    for (let i = 1; i < columns; i++) {
        let diagonal = '';
        for(j = rows - 1, d = i; d < columns; j--, d++){
            diagonal += dna[j][d];
        }
    }

    return diagonals;


}
const findMutations = (dna) => {

    let mutations = 0;

    for(let i = 0; i < dna.length; i++) {
        const row = dna[i]; //Get the row
        for(let j = 0; j < dna[i].length; j++) {
            const positionToEvaluate = row[j]; //Get the letter to evaluate
            let limit = j + 3; 
            if(limit < dna[i].length) { //Verify if there is 3 more positions for evaluating
                let equals = 0;
                for(let p = j + 1; p <= limit; p++) {
                    const newPosition = row[p];
                    if(positionToEvaluate === newPosition) equals++;
                } 
                if( equals === 3 ) mutations ++;
            }
        }
    }
    
    return mutations;
}

const hasMutationHorizontal = (dna) => {

    const horizontalMutation = findMutations(dna);
    return horizontalMutation;

}
const hasMutationVertical = (dna) => {

    const verticalDNA = transposeMatrix(dna);
    const verticalMutations = findMutations(verticalDNA);

    return verticalMutations;

}
const hasMutationObliqueR = (dna) => {

    const diagonals = getDiagonals(dna);
    const diagonalRightMutations = findMutations(diagonals);
    
    return diagonalRightMutations;
    
}
const hasMutationObliqueL = (dna) => {

    const dnaLeft = reverseMatrix(dna);
    const diagonalsLeft = getDiagonals(dnaLeft);
    const diagonalsLeftMutations = findMutations(diagonalsLeft);
    
    return diagonalsLeftMutations;
    
}
const hasMutation = (dna) => {

    

    const mutations = hasMutationHorizontal(dna) + 
                        hasMutationVertical(dna) +
                        hasMutationObliqueL(dna) +
                        hasMutationObliqueR(dna);

    if(mutations > 1) return true;
    return false;

}

module.exports = hasMutation;