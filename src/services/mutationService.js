const hashMutation = require("../helpers/index");
const client = require('../database/config');

const getHasMutation = async (dna) => {
  
    const dnaHasMutation = hashMutation(dna)
    try {

        if(dnaHasMutation) {

            await client.HINCRBY('stats', 'count_mutations', 1);
            return true;
    
        } else {
    
            await client.HINCRBY('stats', 'count_no_mutations', 1)
            return false;
        };

    } catch (error) {
        
        throw error
    }

};

const getStats = async () => {

    try {
        
        const count_mutations = await client.HGET('stats', 'count_mutations');
        const count_no_mutations = await client.HGET('stats', 'count_no_mutations');
        const ratio = Number(count_mutations/count_no_mutations).toFixed(2);

        const response = {
            count_mutations,
            count_no_mutations,
            ratio
        }

        return response;
    } catch (error) {
        
        throw error;
    }
}
module.exports = { getHasMutation, getStats };