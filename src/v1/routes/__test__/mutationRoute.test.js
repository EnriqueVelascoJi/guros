const app = require('../../../../index');
const request = require('supertest');

describe('POST /mutation', () => {
    it('return status code 200 if the DNA matrix has mutation, data response is true', async () => {

        const res = await request(app).post('/api/v1/mutation').send({
            dna:["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        });

        expect(res.statusCode).toEqual(200);
    });

    it('return status code 403 if the DNA matrix has no mutation, but data response is false', async () => {

        const res = await request(app).post('/api/v1/mutation').send({
            dna:["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA","TCACTG"]
        });

        expect(res.statusCode).toEqual(403);
    });

    it('return status code 400 if the DNA argument is missing', async () => {

        const res = await request(app).post('/api/v1/mutation').send({});

        expect(res.statusCode).toEqual(400);
        
    });

    
});

describe('GET /stats', () => {
    it('return status code 200 and stats', async () => {

        const res = await request(app).get('/api/v1/stats');

        expect(res.statusCode).toEqual(200);
    });
});