const express = require("express");
const mutationController = require("../../controllers/mutationController");

const router = express.Router();



/**
 * @openapi
 * /api/v1/mutation:
 *   post:
 *     tags:
 *       - Mutations
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *          schema:
 *            type: object
 *            properities:
 *              dna:
 *               type: array
 *          example:
 *            dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   example: OK
 *                 data:
 *                   type: boolean
 *       403:
 *         description: FAILED
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   example: FAILED
 *                 data:
 *                   type: boolean
 *                   example: false
 */

/**
 * @openapi
 * /api/v1/stats:
 *   get:
 *     tags:
 *       - Mutations
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   example: OK
 *                 data:
 *                   type: object
 
 */
router
  .get('/stats', mutationController.getStats)
  .post('/mutation', mutationController.getResultMutation);

module.exports = router;