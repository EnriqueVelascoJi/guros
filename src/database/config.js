const redis = require('redis');
require('dotenv').config();

console.log(process.env.REDIS_HOST)
const client = redis.createClient({
    password: `${process.env.REDIS_PASSWORD}`,
    socket: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        
    }
});

client.on('error', err => console.log('Redis Client Error', err));


const connectRedis = async() => {
    
    await client.connect();

    const isStats = await client.EXISTS('stats');

    if(!isStats) {
        // Initial values
        await client.HSET('stats', 'count_mutations', 0);
        await client.HSET('stats', 'count_no_mutations', 0);
 
    }
};
connectRedis();

module.exports = client; 


