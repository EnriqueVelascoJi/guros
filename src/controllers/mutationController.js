const mutationService = require('../services/mutationService');


const getResultMutation = async (req, res) => {

    const { dna } = req.body;

    if (!dna) {
        res.status(400).json({
        status: "FAILED",
        data: "Parameter : 'dna' can not be empty" ,
        });
        return;
    }

    //Request to service
    let result;

    try {
        result = await mutationService.getHasMutation(dna);
        if(result)
            res.status(200).json({
                status: "OK",
                data: result,
            });
        else
            res.status(403).json({
                status: "OK",
                data: result,
            });
    } catch (error) {
        res.status(400).json({
            status: "FAILED",
            data: "Something was wrong" ,
            });
    }
  
};

const getStats = async (req, res) => {

    
    let result;

    try {
        result = await mutationService.getStats();
        res.status(200).json({
            status: "OK",
            data: result,
            });
        
    } catch (error) {
        res.status(400).json({
            status: "FAILED",
            data: "Something was wrong" ,
            });
    }
    
}
module.exports = { getResultMutation, getStats };