# Guros


# Environment vars
This project uses the following environment variables:

| Name                          | Description                         | Default Value                                  |
| ----------------------------- | ------------------------------------| -----------------------------------------------|
|PORT                           | Server port                         | 4001      |
|REDIS_PORT                     | PORT for redis instance             | 6379   |
|REDIS_HOST                     | Url public                          | localhost     |
|REDIS_PASSWORD                 | Password for redis connection       | null      |




# Pre-requisites
- Install [Node.js](https://nodejs.org/en/) version 8.0.0
- Install [Redis](https://redis.io/docs/getting-started/installation/)


# Getting started
- Clone the repository
```
git clone  https://gitlab.com/EnriqueVelascoJi/guros.git
```
- Install dependencies
```
cd guros
npm install
```

- Run in dev the project
```
npm run dev
```
- Build and run the project
```
npm start
```
  Navigate to `http://localhost:4001`

- API Document endpoints

  swagger-ui  Endpoint : http://localhost:4001/api/v1/



## Project Structure
The folder structure of this app is explained below:

| Name | Description |
| ------------------------ | --------------------------------------------------------------------------------------------- |
| **node_modules**         | Contains all  npm dependencies                                                            |
| **src**                  | Contains  source code                        |
| **src/controllers**      | Controllers define functions to serve various express routes. 
| **src/database**         | Config for DB.
| **src/services**         | Contain all express services.
| **src/helpers**          | Contain all utils functions.
| **src/database**         | Config for DB.  
| **src/v1/routes**        | Contain all express routes for V1. 
| **./index.js**           | Entry point to express app.                                                              |
| package.json             | Contains npm dependencies |
| .env.example             | Is a template for environment variables                                                              |


### Running tests using NPM Scripts
````
npm run test

````

### Deploy

For deploying was used CI/CD pipeline (Render + Gitlab)

#### Production

- Documentation: https://guros.onrender.com/api/v1/docs/
- Production: https://guros.onrender.com/
- /stats: https://guros.onrender.com/api/v1/stats
- /mutation: https://guros.onrender.com/api/v1/mutation


