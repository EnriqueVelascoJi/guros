//NPM MOodules
const express = require('express');
const morgan = require('morgan');

//Import Router
const v1MuationRouter = require('./src/v1/routes/mutationRoutes');
//Import swagger
const { swaggerDocs: V1SwaggerDocs } = require('./src/v1/swagger');

//Import DB
const cliente = require('./src/database/config')
require('dotenv').config();


//Create the server
const app = express();

//Settings
const PORT = process.env.PORT || 8080;

//Middleware
app.use(morgan());
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello Guros, I am Enrique Velasco Jimenez!')
});
app.use("/api/v1", v1MuationRouter);


// Run server
app.listen(PORT, () => {
    console.log('App running on port: ', PORT);
    V1SwaggerDocs(app, PORT);
});

module.exports = app;


